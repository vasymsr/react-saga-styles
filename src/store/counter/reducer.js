import {INCREMENT, INCREMENT_REQUEST} from "../constant";
const initialState = {
  count: 0,
  countLoading: false,
}

export const incrementRequestAC = () => {
  return {
    type: INCREMENT_REQUEST
  }
};

export const incrementAC = (newCount) => {
  return {
    type: INCREMENT,
    payload: newCount
  }
};

export default function (state = initialState, action) {
  switch(action.type) {
    case INCREMENT: {
      return {...state, count: state.count + action.payload}
    }
    case INCREMENT_REQUEST: {
      return {
        ...state, countLoading: true,
      }
    }
    default: return {...state};
  }
}
