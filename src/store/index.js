import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga'
import counter from './counter/reducer';

import { watchIncrementAsync } from './sagas'

const sagaMiddleware = createSagaMiddleware();

const rootReducer = combineReducers({
  counter,
});


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


export default createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(sagaMiddleware))
)

sagaMiddleware.run(watchIncrementAsync);
