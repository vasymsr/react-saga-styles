import { put, call, takeEvery, takeLatest } from 'redux-saga/effects'
import {INCREMENT_REQUEST} from "./constant";
import {incrementAC} from "./counter/reducer";

const delay = (ms = 1200) => new Promise(res => setTimeout(() => res(Math.random()), ms));

// Our worker Saga: will perform the async increment task
export function* incrementAsync() {
  const result = yield call(delay);
  yield put(incrementAC(result))
}

// Our watcher Saga: spawn a new incrementAsync task on each INCREMENT_ASYNC
export function* watchIncrementAsync() {
  yield takeEvery(INCREMENT_REQUEST, incrementAsync)
}
