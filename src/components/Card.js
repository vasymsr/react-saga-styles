import React from 'react';
import BaseInput from "./BaseInput";

const Card = React.forwardRef((props, ref) =>  {
  const firstInputRef = React.createRef();
  // const secondInputRef = React.createRef();

  React.useEffect(() => {
    console.log('f', firstInputRef);
    console.log('s', props.secondInputRef);
  }, [firstInputRef, props.secondInputRef]);

  return <div>
    <BaseInput ref={firstInputRef} />
    <BaseInput ref={props.secondInputRef}/>
  </div>
});

export default Card;
