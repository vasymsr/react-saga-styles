import React from 'react';

const BaseInput = React.forwardRef((props, ref) => {
  return <label>
    <input
      ref={ref}
      onChange={props.onChange}
      value={props.value}
    />
  </label>
});

export default BaseInput;
