import './App.css';
import MetaTags from 'react-meta-tags';
// import {useDispatch, useSelector} from "react-redux";
// import {INCREMENT_REQUEST} from "./store/constant";
// import {incrementAC, incrementRequestAC} from "./store/counter/reducer";
// import {incrementAsync} from "./store/sagas";
import styled, { css } from 'styled-components'
import React, { useState, useEffect } from "react";
import Card from "./components/Card";


const StyledButton = styled.button`
  background: transparent;
  border-radius: 3px;
  border: 2px solid palevioletred;
  color: palevioletred;
  margin: 0 1em;
  padding: 0.25em 1em;
  &:hover {
    opacity: 0.8;
  }
  
  
  ${props =>
    props.primary &&
    css`
      background: palevioletred;
      color: white;
    `
};
  }
`;

const StyledRoot = styled.div`
  background: aliceblue;
  
  .something-else {
    color: red;
  }
`;



const StyledTomatoButton = styled(StyledButton)`
  color: tomato;
  border-color: tomato;
`;


function useMounted() {
  const mounted = React.useRef(false);
  useEffect(() => {
    if(!mounted.current) {
      mounted.current = true;
    }
  }, []);
  return mounted.current;
};

function App() {
  // const dispatch = useDispatch();
  // const count = useSelector(state => state.counter.count);
  const [title, setTitle] = React.useState('React App');
  const [description, setDescription] = React.useState('React App');

  const mounted = useMounted();
  const [a, setA] = React.useState({name: 2});

  const secondInputRef = React.createRef();
  // const a = React.useMemo(() => secondInputRef, [secondInputRef]);
  React.useEffect(() => {
    console.log('before', mounted);
    if(mounted) {
      console.log('sec', secondInputRef);
    }
  }, [mounted, secondInputRef]);

  React.useEffect(() => {
    setTimeout(() => {
      setTitle('Main App');
      setDescription('Some new description');
    }, 2000);
  }, []);

  React.useEffect(() => {
    setTimeout(() => {
      setTitle('Maiwn App');
      setDescription('Some new description');
    }, 4000);
  }, []);


  return (
    <>
      <MetaTags>
        <title>{ title }</title>
        <meta name='description' content={description} />
      </MetaTags>
      <StyledRoot>
        <Card secondInputRef={secondInputRef}/>
        <div className='something-else'>Some text</div>
        <StyledButton>Button</StyledButton>
        <StyledButton primary>Primary Button</StyledButton>
        <StyledTomatoButton as='a'>Tomato</StyledTomatoButton>
      </StyledRoot>
    </>
  );
}

export default App;
